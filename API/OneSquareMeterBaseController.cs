﻿using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Core.Helpers;
using Core.Helpers.ResponseMessages;
using Core.Services;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace API
{
    [ApiController]
    public class OneSquareMeterBaseController : ControllerBase
    {
        private readonly string _controllerName;
        protected string CurrentUserId => $"({(!string.IsNullOrEmpty(User.Identity.Name) ? User.Identity.Name : "Anonymous")})";
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public OneSquareMeterBaseController(string controllerName)
        {
            _controllerName = controllerName;
            logger.Info(controllerName);
        }

        protected async Task<IServiceResponse<T>> HandleApiOperationAsync<T>(
           Func<Task<ServiceReponse<T>>> action, [CallerLineNumber] int lineNo = 0, [CallerMemberName] string method = "")
        {
            var apiResponse = new ServiceReponse<T> { Status = $"{(int)HttpStatusCode.OK}", Message = "Success" };

            logger.Info($"{_controllerName} / {method} - {CurrentUserId}");

            logger.Info($">>>=============== ENTERS ({method}) ===============>>> ");

            try
            {
                if (!ModelState.IsValid)
                    throw new OneSquareMeterBadRequestMessage("There were errors in your input, please correct them and try again.", $"{(int)HttpStatusCode.BadRequest}");

                var methodResponse = await action.Invoke();

                apiResponse.Data = methodResponse.Data;
                apiResponse.Message = string.IsNullOrEmpty(methodResponse.Message)
                    ? apiResponse.Message
                    : methodResponse.Message;

            }
            catch (OneSquareMeterBadRequestMessage OneSquareMeter)
            {
                logger.Warn($"L{lineNo} - {OneSquareMeter.ErrorCode}: {OneSquareMeter.Message}");
                apiResponse.Message = OneSquareMeter.Message;
                apiResponse.Status = $"{(int)HttpStatusCode.BadRequest}";
            }

            catch (OneSquareMeterNotFoundMessage OneSquareMeter)
            {
                logger.Warn($"L{lineNo} - {OneSquareMeter.ErrorCode}: {OneSquareMeter.Message}");
                apiResponse.Message = OneSquareMeter.Message;
                apiResponse.Status = $"{(int)HttpStatusCode.NotFound}";
            }
            catch (Exception ex)
            {
                logger.Warn($"L{lineNo}: {ex.Message}");
                apiResponse.Message = ex.Message;
                apiResponse.Status = $"{(int)HttpStatusCode.InternalServerError}";
                logger.Error(ex.Message, ex);
            }
            logger.Info($"<<<=============== EXITS ({method}) ===============<<< ");

            return apiResponse;
        }
    }
}
