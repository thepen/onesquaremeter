﻿using System;
using Core.Services;

namespace Services
{
    public class ServiceReponse<TResponse> : IServiceResponse<TResponse>
    {
        public ServiceReponse(TResponse response)
        {
            Data = response;

        }

        public ServiceReponse()
        {

        }

        public string Status { get; set; }
        public string Message { get; set; }
        public TResponse Data { get; set; }
    }
}
