﻿using System;
namespace Core.Helpers.ResponseMessages
{
    public class OneSquareMeterBadRequestMessage : Exception
    {
        public string ErrorCode { get; set; }
        public OneSquareMeterBadRequestMessage(string message) : base(message)
        {

        }

        public OneSquareMeterBadRequestMessage(string message, string errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
