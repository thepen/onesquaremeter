﻿using System;
namespace Core.Helpers.ResponseMessages
{
    public class OneSquareMeterNotFoundMessage : Exception
    {
        public string ErrorCode { get; set; }
        public OneSquareMeterNotFoundMessage(string message) : base(message)
        {

        }

        public OneSquareMeterNotFoundMessage(string message, string errorCode) : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
