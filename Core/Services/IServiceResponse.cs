﻿using System;
namespace Core.Services
{
    public interface IServiceResponse<TReponse>
    {
        string Status { get; set; }
        string Message { get; set; }
        TReponse Data { get; set; }
    }
}
